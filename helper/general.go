package helper

import (
	"os"
	"regexp"
)

// Get the project path by finding the project name in the current working directory.
func GetProjectPath() string {
	projectName := regexp.MustCompile(`^(.*golang-gin)`)
	currentWorkDirectory, _ := os.Getwd()
	return string(projectName.Find([]byte(currentWorkDirectory)))
}

func GetIncrFunc(initial int) func() int {
	initialValue := initial

	return func() int {
		initialValue++
		return initialValue
	}
}
