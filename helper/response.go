package helper

import (
	"reflect"

	"github.com/gin-gonic/gin"
)

type Response[T any] struct {
	Data T `json:"data"`
	Result bool `json:"result"`
	Message string `json:"message,omitempty"`
}

func GenerateJSONResponse[T any](c *gin.Context,response Response[T]) {
	if(response.Result) {
		c.JSON(200, response)
	} else {
		c.JSON(400, response)
	}
}

func GenerateJSONListResponse[T any](c *gin.Context,response Response[T]) {
	if(!response.Result) {
		c.JSON(400, response)
	}
	if(reflect.TypeOf(response.Data).Kind() == reflect.Slice && reflect.ValueOf(response.Data).Len() > 0) {
		c.JSON(200, response)
	}
	c.JSON(200, map[string]interface{}{
		"data": []map[string]interface{}{},
		"result": true,
	})
}