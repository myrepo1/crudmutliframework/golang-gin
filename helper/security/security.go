package security

import (
	"fmt"	
	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"

	"multi-framework/golang-gin/config"
)

type JwtCustomClaim struct {
	TokenType string `json:"tokenType"`
	jwt.RegisteredClaims
}

func HashPassword(password string) (string, error) {
	hashedDummyPass,err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedDummyPass), nil
}

func ComparePassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func GenerateJWT(payload JwtCustomClaim) (string, error) {
	env := config.GetEnv()
	return jwt.NewWithClaims(jwt.SigningMethodHS256, payload).SignedString([]byte(env.JwtSecret))
}

func ValidateJWT(token string) (*JwtCustomClaim, error) {
	env := config.GetEnv()
	parsedToken,err := jwt.ParseWithClaims(token, &JwtCustomClaim{},func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(env.JwtSecret), nil
	})
	if err != nil {
		return &JwtCustomClaim{}, err
	}
	return parsedToken.Claims.(*JwtCustomClaim), nil
}