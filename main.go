package main

import (
	"multi-framework/golang-gin/config"
	"multi-framework/golang-gin/route"
	"multi-framework/golang-gin/db"
)


func main() {	
	env:= config.LoadEnv()
	db.Migrate()
	conn := db.GetPgConnection()

	// Set up connection
	conn.Connect()

	// Close connection when finished
	defer conn.Close()

    app:= route.GetApp()
    app.Run(":"+env.Port)
}