package db

import (
	"context"
	"fmt"
	"os"
	"github.com/vingarcia/ksql"
	"github.com/vingarcia/ksql/adapters/kpgx"
	"multi-framework/golang-gin/config"
)

type PgConnection struct {
	DbUrl string
	Pool ksql.DB
}

var pgConnection PgConnection

func GetPgConnection() *PgConnection {
	return &pgConnection
}

func (conn *PgConnection) Connect() {
	env := config.GetEnv()
	
	pool, err := kpgx.New(context.Background(), env.DbUrl, ksql.Config{})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Connected to database")
	conn.Pool = pool
}

func (conn *PgConnection) Close() {
	fmt.Printf("Closing connection...\n")
	conn.Pool.Close()
}