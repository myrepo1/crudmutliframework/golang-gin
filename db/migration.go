package db

import (	
	"net/url"

	"github.com/amacneil/dbmate/v2/pkg/dbmate"
	_ "github.com/amacneil/dbmate/v2/pkg/driver/postgres"

	"multi-framework/golang-gin/config"
)

func Migrate() {
	env := config.GetEnv()
	u, _ := url.Parse(env.DbUrl)
	db := dbmate.New(u)
	db.Wait()
	err := db.CreateAndMigrate()
	if err != nil {
		panic(err)
	}
}