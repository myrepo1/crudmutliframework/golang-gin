package config

import (
	"fmt"
	"github.com/spf13/viper"

	"multi-framework/golang-gin/helper"
)

type EnvConfig struct {
	Port string
	DbUrl string
	JwtSecret string
}

var env EnvConfig

func LoadEnv() EnvConfig {
	generateDefaultEnv()
	projectRootPath := helper.GetProjectPath() + "/app.env"
	viper.SetConfigFile(projectRootPath)
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}
	env := generateValues()
	return env
}

func GetEnv() *EnvConfig {
	return &env
}

func generateValues() EnvConfig {
	env = EnvConfig{
		Port: viper.GetString("PORT"),
		DbUrl: viper.GetString("DATABASE_URL"),
		JwtSecret: viper.GetString("JWT_SECRET"),
	}
	return env
}

func generateDefaultEnv() {
	viper.SetDefault("PORT", "4000")
	viper.SetDefault("DATABASE_URL","")
	viper.SetDefault("JWT_SECRET","secret")
}