package route

import (
	"strconv"

	"github.com/gin-gonic/gin"

	"multi-framework/golang-gin/helper"
	"multi-framework/golang-gin/middleware"
	"multi-framework/golang-gin/service"
)

func Category(rg *gin.RouterGroup) {
	rg.GET("/", func(c *gin.Context) {
		response := service.GetCategories(c)
		if response.Result {
			c.JSON(200, response)
		} else {
			c.JSON(400, response)
		}
	})
	rg.POST("/", middleware.JWT(), func(c *gin.Context) {
		var payload = service.Category{}
		if c.ShouldBindJSON(&payload) == nil {
			response := service.CreateCategory(c, payload)
			if response.Result {
				c.JSON(201, response)
			} else {
				c.JSON(400, response)
			}
		} else {
			c.JSON(400, helper.Response[string]{
				Result:  false,
				Message: "Bad Request",
			})
		}
	})
	rg.PUT("/category/:id", middleware.JWT(), func(c *gin.Context) {
		var payload = make(map[string]any)
		if c.ShouldBindJSON(&payload) == nil {
			id, _ := strconv.Atoi(c.Param("id"))
			response := service.UpdateCategory(c, payload, id)
			if response.Result {
				c.JSON(200, response)
			} else {
				c.JSON(400, response)
			}
		} else {
			c.JSON(400, helper.Response[string]{
				Result:  false,
				Message: "Bad Request",
			})
		}
	})

	rg.DELETE("/category/:id", middleware.JWT(), func(c *gin.Context) {
		id, _ := strconv.Atoi(c.Param("id"))
		response := service.DeleteCategory(c, id)
		if response.Result {
			c.JSON(200, response)
		} else {
			c.JSON(400, response)
		}
	})
}
