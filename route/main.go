package route

import "github.com/gin-gonic/gin"


func GetApp() *gin.Engine {
	app := gin.Default()

	app.GET("/health", func(c *gin.Context) {
		c.IndentedJSON(200, gin.H{
			"status": "ok",
		})
	})

	Auth(app.Group("/auth"))
	Category(app.Group("/categories"))

	return app
}