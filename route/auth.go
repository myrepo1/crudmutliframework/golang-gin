package route

import (
	"github.com/gin-gonic/gin"
	
	"multi-framework/golang-gin/helper"
	"multi-framework/golang-gin/service"
)

func Auth(rg *gin.RouterGroup) {
	rg.POST("/login",func(c *gin.Context) {
		var payload = service.LoginData{}
		if(c.ShouldBindJSON(&payload) == nil) {
			response := service.Login(payload)
			if(response.Result) {
				c.JSON(200, response)
			} else {
				c.JSON(400, response)
			}
		} else {
			c.JSON(400, helper.Response[string]{
				Result: false,
				Message: "Bad Request",
			})
		}
	})

	rg.POST("/refresh", func(c *gin.Context) {
		var payload = service.RefreshTokenData{}
		if(c.ShouldBindJSON(&payload) == nil) {
			response := service.Refresh(payload.RefreshToken)
			if(response.Result) {
				c.JSON(200, response)
			} else {
				c.JSON(400, response)
			}
		} else {
			c.JSON(400, helper.Response[string]{
				Result: false,
				Message: "Bad Request",
			})
		}
	})
}