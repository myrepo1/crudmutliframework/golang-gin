package service

import (
	"time"
	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"

	"multi-framework/golang-gin/helper"
	"multi-framework/golang-gin/helper/security"
)

type LoginData struct {
	Username string `json:"username" validator:"required"`
	Password string `json:"password" validator:"required"`
}

type RefreshTokenData struct {
	RefreshToken string `json:"refreshToken" validator:"required"`
}

type LoginResponseData struct {
	AccessToken string `json:"accessToken,omitempty"`
	RefreshToken string `json:"refreshToken,omitempty"`
}

var dummyUser = LoginData {
	Username: "admin",
	Password: "admin",
}

func Login(data LoginData) helper.Response[*LoginResponseData] {
	validate := validator.New()
	err := validate.Struct(data)
	if err != nil {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: "Login failed",
		}
	}
	hashedDummyPass,err := bcrypt.GenerateFromPassword([]byte(dummyUser.Password), bcrypt.DefaultCost)
	if err != nil {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: err.Error(),
		}
	}
	err = bcrypt.CompareHashAndPassword(hashedDummyPass, []byte(data.Password))
	if err != nil || data.Username != dummyUser.Username {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: "Login failed",
		}
	}
	

	accessTokenClaim := security.JwtCustomClaim{
		TokenType: "accessToken",
		RegisteredClaims: jwt.RegisteredClaims{
			ID:data.Username,
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	}

	refreshTokenClaim := security.JwtCustomClaim{
		TokenType: "refreshToken",
		RegisteredClaims: jwt.RegisteredClaims{
			ID:data.Username,
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(7 * 24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	}

	accessToken,accessTokenErr := security.GenerateJWT(accessTokenClaim)
	refreshToken,refreshTokenErr := security.GenerateJWT(refreshTokenClaim)

	if accessTokenErr != nil || refreshTokenErr != nil {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: "Failed generate token",
		}
	}

	return helper.Response[*LoginResponseData]{
		Result: true,
		Data: &LoginResponseData{
			AccessToken: accessToken,
			RefreshToken: refreshToken,
		},
	}

}

func Refresh(token string) helper.Response[*LoginResponseData] {
	if(len(token) == 0) {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: "Invalid Token",
		}
	}
	claim,err := security.ValidateJWT(token)
	if err != nil {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: err.Error(),
		}
	}
	if claim.TokenType != "refreshToken" {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: "Invalid Token",
		}
	}

	accessTokenClaim := security.JwtCustomClaim{
		TokenType: "accessToken",
		RegisteredClaims: jwt.RegisteredClaims{
			ID:claim.ID,
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	}

	accessToken,accessTokenErr := security.GenerateJWT(accessTokenClaim)

	if accessTokenErr != nil {
		return helper.Response[*LoginResponseData]{
			Result: false,
			Message: "Failed generate token",
		}
	}

	return helper.Response[*LoginResponseData]{
		Result: true,
		Data: &LoginResponseData{
			AccessToken: accessToken,
			RefreshToken: token,
		},
	}
}