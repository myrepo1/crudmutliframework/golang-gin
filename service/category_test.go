package service

import (
	"context"
	"log"
	"testing"

	"multi-framework/golang-gin/config"
	"multi-framework/golang-gin/db"

	"github.com/vingarcia/ksql"
)

func TestCategoryService(t *testing.T) {
	teardownSuite := setupSuite(t)
	defer teardownSuite(t)
	pool := db.GetPgConnection().Pool
	ctx := context.Background()
	t.Run("SuccessGetCategories", func(t *testing.T) {
		teardownTest := setupTest(t, false)
		defer teardownTest(t)
		result := GetCategories(ctx)
		if result.Result == false {
			log.Println(result.Message)
			t.FailNow()
		}
		if len(result.Data) == 0 {
			log.Println("Empty data")
			t.FailNow()
		}
		if result.Data[0].Name != "test" {
			log.Println("Wrong data")
			t.FailNow()
		}
		log.Println("Success Get Categories")
	})
	t.Run("SuccessGetEmptyCategories", func(t *testing.T) {
		result := GetCategories(ctx)
		if result.Result == false {
			t.FailNow()
		}
		if len(result.Data) != 0 {
			t.FailNow()
		}
		log.Println("Success Get Empty Categories")
	})
	t.Run(("SuccessCreateCategory"), func(t *testing.T) {
		teardownTest := setupTest(t, true)
		defer teardownTest(t)
		result := CreateCategory(ctx, Category{
			Name:        "newCategory",
			Description: "test",
		})
		if result.Result == false {
			log.Println(result.Message)
			t.FailNow()
		}
		if result.Data.Name != "newCategory" {
			log.Println("Wrong data")
			t.FailNow()
		}
		var category Category
		err := pool.QueryOne(ctx, &category, "SELECT * FROM category WHERE id = $1", result.Data.Id)
		if err != nil {
			log.Println(err)
			t.FailNow()
		}
		if category.Name != "newCategory" {
			log.Println("Wrong data")
			t.FailNow()
		}
		log.Println("Success create category")
	})
	t.Run("FailCreateCategoryGivenEmptyName", func(t *testing.T) {
		teardownTest := setupTest(t, true)
		defer teardownTest(t)
		result := CreateCategory(ctx, Category{
			Name:        "",
			Description: "test",
		})
		if result.Result == true {
			log.Println(result.Message)
			t.FailNow()
		}
		log.Println("Fail create category given empty name")
	})
	t.Run("SuccessUpdateCategory", func(t *testing.T) {
		teardownTest := setupTest(t, false)
		defer teardownTest(t)
		var currentCategory Category
		err := pool.QueryOne(ctx, &currentCategory, "SELECT * FROM category LIMIT 1")
		if err != nil {
			log.Println(err)
			t.FailNow()
		}
		result := UpdateCategory(ctx, map[string]any{"name": "newName"}, currentCategory.Id)
		if result.Result == false {
			log.Println(result.Message)
			t.FailNow()
		}
		if result.Data.Name != "newName" {
			log.Println("Wrong data")
			t.FailNow()
		}
		var updatedCategory Category
		err = pool.QueryOne(ctx, &updatedCategory, "SELECT * FROM category WHERE id = $1", currentCategory.Id)
		if err != nil {
			log.Println(err)
			t.FailNow()
		}
		if updatedCategory.Name != "newName" {
			log.Println("Wrong data")
			t.FailNow()
		}
		log.Println("Success update category")
	})
	t.Run("FailUpdateCategoryGivenEmptyName", func(t *testing.T) {
		teardownTest := setupTest(t, false)
		defer teardownTest(t)
		var currentCategory Category
		err := pool.QueryOne(ctx, &currentCategory, "SELECT * FROM category LIMIT 1")
		if err != nil {
			log.Println(err)
			t.FailNow()
		}
		result := UpdateCategory(ctx, map[string]any{"name": ""}, currentCategory.Id)
		if result.Result == true {
			log.Println("Category updated with empty name")
			t.FailNow()
		}
		log.Println("Fail update category given empty name")
	})
	t.Run("SuccessDeleteCategory", func(t *testing.T) {
		teardownTest := setupTest(t, false)
		defer teardownTest(t)
		var currentCategory Category
		err := pool.QueryOne(ctx, &currentCategory, "SELECT * FROM category LIMIT 1")
		if err != nil {
			log.Println(err)
			t.FailNow()
		}
		result := DeleteCategory(ctx, currentCategory.Id)
		if result.Result == false {
			log.Println(result.Message)
			t.FailNow()
		}
		var deletedCategory []Category
		err = pool.Query(ctx, &deletedCategory, "SELECT * FROM category WHERE id = $1", currentCategory.Id)
		if err != nil {
			log.Println(err)
			t.FailNow()
		}
		if len(deletedCategory) != 0 {
			log.Println("Category not deleted")
			t.FailNow()
		}
		log.Println("Success delete category")
	})
}

func setupSuite(t *testing.T) func(t *testing.T) {
	log.Println("setup category suite")
	config.LoadEnv()
	conn := db.GetPgConnection()
	conn.Connect()

	_, err := conn.Pool.Exec(context.Background(), "DELETE FROM category")
	if err != nil {
		t.Fail()
	}
	return func(t *testing.T) {
		conn.Close()
		log.Println("teardown category suite")
	}
}

func setupTest(t *testing.T, skipPopulateCategory bool) func(t *testing.T) {
	log.Println("setup category test")
	if !skipPopulateCategory {
		err := prepareCategories()
		if err != nil {
			t.Fail()
		}
	}
	return func(t *testing.T) {
		log.Println("teardown category test")
		err := clearCategories()
		if err != nil {
			t.Fail()
		}
	}
}

func prepareCategories() error {
	pool := db.GetPgConnection().Pool
	var categoryTable = ksql.NewTable("category")
	err := pool.Insert(context.Background(), categoryTable, &Category{
		Name:        "test",
		Description: "test",
	})
	return err
}

func clearCategories() error {
	pool := db.GetPgConnection().Pool
	_, err := pool.Exec(context.Background(), "DELETE FROM category")
	return err
}
