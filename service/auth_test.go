package service

import (
	"multi-framework/golang-gin/helper/security"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

func TestSuccessLogin(t *testing.T) {
	result := Login(LoginData{
		Username: "admin",
		Password: "admin",
	})
	if(result.Result == false || result.Message != "") {
		t.Fail()
	}
	if(result.Data.AccessToken == "" || result.Data.RefreshToken == "") {
		t.Fatalf("token is empty")
	}
	claim,err := security.ValidateJWT(result.Data.AccessToken)
	if(err != nil || claim.TokenType != "accessToken") {
		t.Fail()
	}
	claim,err = security.ValidateJWT(result.Data.RefreshToken)
	if(err != nil || claim.TokenType != "refreshToken") {
		t.Fail()
	}
}

func TestFailedLoginInvalidPassword(t *testing.T) {
	result := Login(LoginData{
		Username: "admin",
		Password: "invalid",
	})
	if(result.Result == true || result.Message == "") {
		t.Fail()
	}
	if(result.Data != nil) {
		t.Fatalf("invalid failed login response data")
	}
}

func TestSuccessRefreshToken(t *testing.T) {
	token,_ := security.GenerateJWT(security.JwtCustomClaim{
		TokenType: "refreshToken",
		RegisteredClaims: jwt.RegisteredClaims{
			ID:"admin",
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(7 * 24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	})
	result := Refresh(token)
	if(result.Result == false || result.Message != "") {
		t.Fail()
	}
	if(result.Data.AccessToken == "" || result.Data.RefreshToken == "") {
		t.Fatalf("token is empty")
	}
	claim,err := security.ValidateJWT(result.Data.AccessToken)
	if(err != nil || claim.TokenType != "accessToken") {
		t.Fail()
	}
	claim,err = security.ValidateJWT(result.Data.RefreshToken)
	if(err != nil || claim.TokenType != "refreshToken") {
		t.Fail()
	}
}

func TestFailedRefreshTokenGivenEmptyToken(t *testing.T) {
	result := Refresh("")
	if(result.Result == true || result.Message == "") {
		t.Fail()
	}
	if(result.Data != nil) {
		t.Fatalf("invalid failed refresh response data")
	}	
}

func TestFailedRefreshTokenGivenWrongTokenType(t *testing.T) {
	token,_ := security.GenerateJWT(security.JwtCustomClaim{
		TokenType: "accessToken",
		RegisteredClaims: jwt.RegisteredClaims{
			ID:"admin",
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(7 * 24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	})
	result := Refresh(token)
	if(result.Result == true || result.Message == "") {
		t.Fail()
	}
	if(result.Data != nil) {
		t.Fatalf("invalid failed refresh response data")
	}	
}