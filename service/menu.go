package service

type Menu struct {
	Id          int    `json:"id" ksql:"id"`
	Name        string `json:"name" ksql:"name" validate:"required,min=1"`
	Description string `json:"description" ksql:"description"`
	Price       int    `json:"price" ksql:"price"`
}
