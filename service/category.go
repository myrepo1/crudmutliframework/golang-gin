package service

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/vingarcia/ksql"

	"multi-framework/golang-gin/db"
	"multi-framework/golang-gin/helper"
)

type Category struct {
	Id          int    `json:"id" ksql:"id"`
	Name        string `json:"name" ksql:"name" validate:"required,min=1"`
	Description string `json:"description" ksql:"description"`
}

func GetCategories(ctx context.Context) helper.Response[[]Category] {
	pool := db.GetPgConnection().Pool
	var categories []Category
	err := pool.Query(ctx, &categories, "SELECT * FROM category")
	if err != nil {
		return helper.Response[[]Category]{
			Result:  false,
			Message: err.Error(),
		}
	}
	if len(categories) == 0 {
		return helper.Response[[]Category]{
			Result: true,
			Data:   []Category{},
		}
	}
	return helper.Response[[]Category]{
		Result: true,
		Data:   categories,
	}
}

func CreateCategory(ctx context.Context, data Category) helper.Response[*Category] {
	validate := validator.New()
	err := validate.Struct(data)
	if err != nil {
		return helper.Response[*Category]{
			Result:  false,
			Message: "Invalid data",
		}
	}
	pool := db.GetPgConnection().Pool
	var categoryTable = ksql.NewTable("category")
	err = pool.Insert(ctx, categoryTable, &data)
	if err != nil {
		fmt.Println(err)
		return helper.Response[*Category]{
			Result:  false,
			Message: "Failed insert data",
		}
	}
	return helper.Response[*Category]{
		Result: true,
		Data:   &data,
	}
}

func UpdateCategory(ctx context.Context, data map[string]any, id int) helper.Response[*Category] {
	if len(data) == 0 {
		return helper.Response[*Category]{
			Result:  false,
			Message: "No data to update",
		}
	}
	setClause := []string{}
	setValue := []any{}
	incrFunc := helper.GetIncrFunc(0)
	for key, element := range data {
		if key == "name" {
			if name, ok := element.(string); !ok || len(name) == 0 {
				return helper.Response[*Category]{
					Result:  false,
					Message: "Name cannot be empty",
				}
			}
			setClause = append(setClause, fmt.Sprintf("name = $%d", incrFunc()))
			setValue = append(setValue, element)
		}
		if key == "description" {
			if _, ok := element.(string); ok && element != nil {
				return helper.Response[*Category]{
					Result:  false,
					Message: "Invalid description value",
				}
			}
			setClause = append(setClause, fmt.Sprintf("description = $%d", incrFunc()))
			setValue = append(setValue, element)
		}
	}
	if len(setClause) == 0 {
		return helper.Response[*Category]{
			Result:  false,
			Message: "No data to update",
		}
	}
	updateQuery := fmt.Sprintf("UPDATE category SET %s WHERE id = $%d RETURNING *", strings.Join(setClause, ", "), incrFunc())
	setValue = append(setValue, id)

	pool := db.GetPgConnection().Pool
	var category Category
	err := pool.QueryOne(ctx, &category, updateQuery, setValue...)
	if err != nil {
		return helper.Response[*Category]{
			Result:  false,
			Message: "Failed update category",
		}
	}
	return helper.Response[*Category]{
		Result: true,
		Data:   &category,
	}
}

func DeleteCategory(ctx context.Context, id int) helper.Response[any] {
	pool := db.GetPgConnection().Pool
	var category Category
	err := pool.QueryOne(ctx, &category, "DELETE FROM category WHERE id = $1 RETURNING *", id)
	if err != nil {
		return helper.Response[any]{
			Result:  false,
			Message: "Failed delete category",
		}
	}
	return helper.Response[any]{
		Result: true,
		Data:   nil,
	}
}
