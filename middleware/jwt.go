package middleware

import (
	"multi-framework/golang-gin/helper"
	"multi-framework/golang-gin/helper/security"
	"strings"

	"github.com/gin-gonic/gin"
)

func JWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("Authorization")
		claim,err := security.ValidateJWT(strings.Replace(token, "Bearer ", "", 1))
		if(err != nil) {
			c.AbortWithStatusJSON(401, helper.Response[string]{
				Result: false,
				Message: err.Error(),
			})
		} else if(claim.TokenType != "accessToken") {
			c.AbortWithStatusJSON(401, helper.Response[string]{
				Result: false,
				Message: "Invalid Token",
			})
		}
		c.Next()
	}
}